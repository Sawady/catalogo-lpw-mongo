package controllers

import models.Music
import models.MusicModel

class MusicCtrl extends Items[Music] {
  
  val model = MusicModel
  val colName = "music"

}