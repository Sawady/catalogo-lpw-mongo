package controllers

import models.Movie
import models.MovieModel

class MoviesCtrl extends Items[Movie] {
  
  val model   = MovieModel
  val colName = "movies"

}
