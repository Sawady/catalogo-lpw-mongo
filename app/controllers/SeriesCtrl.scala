package controllers

import models.Serie
import models.SerieModel

class SeriesCtrl extends Items[Serie] {
  
  val model = SerieModel
  val colName = "series"

}