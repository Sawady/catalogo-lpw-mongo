package controllers

import models.Soft
import models.SoftModel

class SoftCtrl extends Items[Soft] {
  
  val model = SoftModel
  val colName = "soft"

}