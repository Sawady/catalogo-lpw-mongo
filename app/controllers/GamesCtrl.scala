package controllers

import models.Game
import models.GameModel

class GamesCtrl extends Items[Game] {
  
  val model = GameModel
  val colName = "games"

}