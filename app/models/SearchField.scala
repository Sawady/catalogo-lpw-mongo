package models

import play.api.libs.json.JsObject
import play.api.libs.json.JsValue
import play.api.libs.json.Json

object SearchField {

  type FieldTransform = (JsValue, String) => (String, JsValue)

  def regex: FieldTransform = (jsVal, k) => (k, Json.obj("$regex" -> jsVal \ k, "$options" -> "i"))
  def in: FieldTransform = (jsVal, k) => (k, Json.obj("$in" -> jsVal \ k))
  def default: FieldTransform = (jsVal, k) => (k, jsVal \ k)

  def toSearchFields(m: Map[String, FieldTransform]): (JsValue, List[String]) => JsObject = {

    return (jsVal, validKeys) =>
      {

        var ob = Json.obj()

        for (vk <- validKeys) {
          ob = ob + m.getOrElse(vk, default)(jsVal, vk)
        }

        ob
      }

  }

}